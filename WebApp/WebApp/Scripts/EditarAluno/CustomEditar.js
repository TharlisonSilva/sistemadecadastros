﻿$(document).ready(function () {
    var select = document.getElementById('Professores-Editar');
    if (select != null) {
        var inputProfessor = document.getElementById('Custom-Professor-Editar');
        var request = $.ajax({
            url: "/Professores/ListarProfessores/",
            dataType: 'json',
        }).done(function (msg) {

            $(msg).each(function (i) {
                var option = document.createElement('option');
                option.value = msg[i].Id;
                option.textContent = msg[i].Nome;
                select.appendChild(option);
                if (inputProfessor.value = msg[i].Id) {
                    option.selected = true;
                }
            });
        }).fail(function (jqXHR, textStatus, msg) {
            alert(msg);
        });

        $("#Custom-DataNascimento-Editar").mask("99/99/9999");

        var Professores = document.getElementById('Professores-Editar');
        Professores.addEventListener('change', function () {
            inputProfessor.value = this.value;
        }, false);
        
    }        
});