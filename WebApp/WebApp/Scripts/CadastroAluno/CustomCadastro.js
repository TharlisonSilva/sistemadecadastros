﻿$(document).ready(function () {

    var select = document.getElementById('Professores');
    if (select != null) {
        var inputProfessor = document.getElementById('Custom-Professor-Cadastro');
        inputProfessor.value = "";

        var a = document.getElementById('Professores');
        a.addEventListener('change', function () {
            inputProfessor.value = this.value;
        }, false);


        var request = $.ajax({
            url: "/Professores/ListarProfessores/",
            dataType: 'json',
        }).done(function (msg) {

            $(msg).each(function (i) {

                var option = document.createElement('option');
                option.value = msg[i].Id;
                option.textContent = msg[i].Nome;
                select.appendChild(option);
            });
        }).fail(function (jqXHR, textStatus, msg) {
            alert(msg);
            });

        $("#Custom-DataNascimento-Cadastro").mask("99/99/9999");
    }
});