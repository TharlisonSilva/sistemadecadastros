﻿$(document).ready(function () {
    var professor = document.getElementById('Custom-Professor-Detalhes');
    if (professor != null) {

        var request = $.ajax({
            url: "/Professores/BuscarProfessorPorId/",
            dataType: 'json',
            data: { 'id': professor.textContent },      
        }).done(function (msg){
            professor.textContent = msg;
        }).fail(function (jqXHR, textStatus, msg) {
            alert(msg);
        });

    }
});