﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WebApp.Models
{
    public class AlunosMaioresDe16Anos
    {
        public string Professor { get; set; }
        public string Aluno { get; set; }
        public int Idade { get; set; }

        public AlunosMaioresDe16Anos() { }

        public AlunosMaioresDe16Anos(string professor, string aluno, int idade)
        {
            this.Professor = professor;
            this.Aluno = aluno;
            this.Idade = idade;
        }

        public List<AlunosMaioresDe16Anos> BuscarAlunos()
        {
            string SQL = @"SELECT B.NOME AS Professor,       
                                  A.NOME AS Aluno, 
	                              DATEDIFF(YEAR, CONVERT(VARCHAR, A.DATANASCIMENTO), CONVERT(VARCHAR, GETDATE())) AS Idade
                            FROM ALUNOES A
                            JOIN PROFESSORS B ON B.ID = A.PROFESSORID
                           GROUP BY B.NOME, A.NOME, A.DATANASCIMENTO
                          HAVING((DATEDIFF(YEAR, CONVERT(VARCHAR, A.DATANASCIMENTO), CONVERT(VARCHAR, GETDATE()))) > 16)";
            try
            {
                using (var consulta = new Contexto())
                {
                    return consulta.Database.SqlQuery<AlunosMaioresDe16Anos>(SQL).ToList();
                }
            }
            catch (Exception e)
            {
                throw new Exception("Erro ao consultar informações. \n Exceção: " + e.Message);
            }
        }
    }
}