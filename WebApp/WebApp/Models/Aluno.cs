﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models
{
    public class Aluno
    {
        [Key]
        [Display(Name ="Id")]
        public int Id { get; set;}

        [Required(ErrorMessage = "Campo é obrigatorio")]
        [Display(Name = "Professor")]
        public int ProfessorId { get; set; }

        [Required(ErrorMessage = "Campo obrigatorio")]
        [Display(Name = "Nome")]
        public string Nome{ get; set; }

        [Required(ErrorMessage = "Campo obrigatorio")]
        [Display(Name = "Data Nascimento")]
        public DateTime DataNascimento { get; set; }
    }
}