﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WebApp.Models
{
    public class MediaAlunos
    {
        public string Professor { get; set; }
        public int MediaIdade { get; set; }

        public MediaAlunos() { }

        public MediaAlunos(string professor, int media)
        {
            this.Professor = professor;
            this.MediaIdade = media;
        }

        public List<MediaAlunos> BuscarAlunos()
        {
            string SQL = @"SELECT B.NOME AS Professor, 
                                   AVG((DATEDIFF(YEAR, CONVERT(VARCHAR, A.DATANASCIMENTO), CONVERT(VARCHAR, GETDATE())))) AS MediaIdade
                              FROM ALUNOES A
                              JOIN PROFESSORS B ON B.ID = A.PROFESSORID
                             GROUP BY B.NOME
                            HAVING((AVG((DATEDIFF(YEAR, CONVERT(VARCHAR, A.DATANASCIMENTO), CONVERT(VARCHAR, GETDATE()))))) BETWEEN 15 AND 17 )";
            try
            {
                using (var consulta = new Contexto())
                {
                    return consulta.Database.SqlQuery<MediaAlunos>(SQL).ToList();
                }
            }
            catch (Exception e)
            {
                throw new Exception("Erro ao consultar informações. \n Exceção: " + e.Message);
            }
        }
    }
}