﻿using System.ComponentModel.DataAnnotations;

namespace WebApp.Models
{
    public class Professor
    {
        [Key]
        [Display(Name = "Id")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Campo é obrigatorio")]
        [Display(Name = "Nome")]
        public string Nome { get; set; }
    }
}