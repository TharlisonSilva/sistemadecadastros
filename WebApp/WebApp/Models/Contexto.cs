﻿using System.Data.Entity;

namespace WebApp.Models
{
    public class Contexto : DbContext
    {
        public DbSet<Professor> Professores { get; set; }
        public DbSet<Aluno> Alunos { get; set; }
    }
}
