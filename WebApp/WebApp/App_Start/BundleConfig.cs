﻿using System.Web;
using System.Web.Optimization;

namespace WebApp
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/selectize.default.css",
                      "~/Content/Custom.css"));

            bundles.Add(new ScriptBundle("~/bundles/Custom").Include(
                    "~/Scripts/jquery.mask.js",
                    "~/Scripts/DetalhesAluno/CustomDetalhes.js",
                    "~/Scripts/HomeAlunos/CustomHome.js",
                    "~/Scripts/EditarAluno/CustomEditar.js",
                    "~/Scripts/CadastroAluno/CustomCadastro.js"));
        }
    }
}
